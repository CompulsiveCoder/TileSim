﻿using System;

namespace townsim.Alerts
{
	public class StarvationAlert : BaseAlert
	{
		public StarvationAlert () : base("People are dying of starvation. There's not enough food.")
		{
		}
	}
}

