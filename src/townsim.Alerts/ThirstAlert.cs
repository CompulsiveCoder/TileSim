﻿using System;

namespace townsim.Alerts
{
	public class ThirstAlert : BaseAlert
	{
		public ThirstAlert () : base("People are thirsty. There's not enough water.")
		{
		}
	}
}

