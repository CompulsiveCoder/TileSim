using System;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace townsim.Entities
{
	public enum EmploymentType
	{
		NotSet,
		Builder,
		Forestry
	}

}

