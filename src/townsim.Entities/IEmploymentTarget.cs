﻿using System;

namespace townsim.Entities
{
	public interface IEmploymentTarget
	{
		Person[] Workers { get;set; }
	}
}

