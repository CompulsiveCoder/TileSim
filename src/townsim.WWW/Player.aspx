﻿<%@ Page Language="C#" %>
<%@ Import Namespace="townsim.Data" %>
<%@ Import Namespace="townsim.Entities" %>
<%@ Register TagPrefix="uc" TagName="Player" Src="~/Panels/PlayerPanel.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Player</title>
</head>
<body>
	<form id="form">
		<uc:Player id="Player" runat="server" />
	</form>
</body>
</html>

